package com.xarrper.olga.myweather;

import java.io.Serializable;

public class Forecast implements Serializable {

    private String dt;
    private String description;
    private int min;
    private int max;
    private int humidity;
    private int speed;
    private int pressure;

    public Forecast(String dt, String description, int speed, int humidity, int pressure, int min, int max) {
        this.dt = dt;
        this.description = description;
        this.min = min;
        this.max = max;
        this.speed = speed;
        this.humidity = humidity;
        this.pressure = pressure;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public String toString() {
        return dt + " - " + description + " - " + min + "/" + max;
    }
}
