package com.xarrper.olga.myweather;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//TODO:
// 1.) вместо String[] используется ArrayList<String>
// 2.) new Http().getUrl(params[0]); стр 87.
// 3.) вопрос на обработку ошибок
public class ForecastFragment extends ListFragment {

    private final static String TAG = "ForecastFragment";
    private final static String DIALOG_CHOICE_CITY = "choice_city";
    private String paramCity;
    private final static int REQUEST_DATE = 0;

    private ArrayList<Forecast> forecastData;
    private ListView listView;
    private SharedPreferences prefs;

    public ForecastFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        prefs = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
        paramCity = prefs.getString("city", "Astrakhan");
        fetchWeatherTaskExecute();

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView)view.findViewById(android.R.id.list);

        return view;
    }

    protected class ForecastAdapter extends ArrayAdapter<Forecast> {

        public ForecastAdapter(ArrayList<Forecast> pointList) {
            super(getActivity(), 0, pointList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_forecast, null);
            }

            final Forecast data = getItem(position);
            TextView nameTextView = (TextView)convertView.findViewById(R.id.list_item_forecast_textView);
            nameTextView.setText(data.toString());

            return convertView;
        }
    }

    private class FetchWeatherTask extends AsyncTask<String,Void,ArrayList<Forecast>> {
        @Override
        protected ArrayList<Forecast> doInBackground(String... params) {
            try {
                String result = new Http().getUrl(params[0]);
                Log.d(TAG, result);
                forecastData = parseWeatherJson(result, 7);
                Log.i(TAG, "Fetched contents of URL: " + result);
            }
            catch (IOException ioe) {
                Log.e(TAG, "Failed to fetch URL: ", ioe);
            }
            catch (JSONException e) {
                Log.d(TAG, "ошибка " + e);
                e.printStackTrace();
            }
            return forecastData;
        }

        @Override
        protected void onPostExecute(ArrayList<Forecast> result) {
            super.onPostExecute(result);

            ForecastAdapter adapter = new ForecastAdapter(forecastData);
            listView.setAdapter(adapter);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.forecast_fragment, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                fetchWeatherTaskExecute();
                return true;
            case R.id.dialog_choice_city:
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                ChoiceCityDialogFragment dialog = new ChoiceCityDialogFragment();
                dialog.setTargetFragment(ForecastFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_CHOICE_CITY);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO: в каком классе? вынести из фрагмента, названи
    private String displayDate(int time) {
        Date date = new Date(time * 1000);
        SimpleDateFormat format = new SimpleDateFormat("E, dd-MM");
        return format.format(date).toString();
    }

    //TODO: в каком классе? вынести из фрагмента, название
    private ArrayList<Forecast> parseWeatherJson(String weatherJson, int numDays) throws JSONException {
        ArrayList<Forecast> forecastData = new ArrayList<Forecast>();

        for (int i = 0; i < numDays; i++) {
            JSONObject data = new JSONObject(weatherJson).getJSONArray("list").getJSONObject(i);

            int dt = data.getInt("dt");
            String description = data.getJSONArray("weather").getJSONObject(0).getString("description");
            int max = data.getJSONObject("temp").getInt("max");
            int min = data.getJSONObject("temp").getInt("min");
            int humidity = data.getInt("humidity");
            int speed = data.getInt("speed");
            int pressure = data.getInt("pressure");

            Forecast forecast = new Forecast(displayDate(dt), description, speed, humidity, pressure, min, max);

            forecastData.add(forecast);
            Log.d(TAG, displayDate(dt) + " - " + description + " - " + min + "/" + max);
        }

        return forecastData;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_DATE) {
            paramCity = data.getStringExtra(ChoiceCityDialogFragment.EXTRA_CITY);
        }
    }

    private void fetchWeatherTaskExecute() {
        Log.d(TAG, paramCity);
        new FetchWeatherTask().execute(paramCity);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Forecast forecast = (Forecast)listView.getItemAtPosition(position);
        Intent i = new Intent(getActivity(), WeatherDetailActivity.class);
        i.putExtra(WeatherDetailActivityFragment.FORECAST, forecast);
        startActivity(i);
    }

}
