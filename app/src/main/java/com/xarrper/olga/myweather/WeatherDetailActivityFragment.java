package com.xarrper.olga.myweather;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WeatherDetailActivityFragment extends Fragment {

    private Forecast forecast;
    public static final String FORECAST = "forecast";

    public WeatherDetailActivityFragment() {
//        forecast = new Forecast("","", 0,0,0,0,0);//(Forecast) getActivity().getIntent().getSerializableExtra(FORECAST);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);

        forecast = (Forecast) getActivity().getIntent().getSerializableExtra(FORECAST);

        TextView dt = (TextView)view.findViewById(R.id.dtInfoTextView);
        dt.setText(forecast.getDt());

        TextView description = (TextView)view.findViewById(R.id.descriptionInfoTextView);
        description.setText(forecast.getDescription());
        TextView min = (TextView)view.findViewById(R.id.minInfoTextView);
        min.setText(Integer.toString(forecast.getMin()));
        TextView max = (TextView)view.findViewById(R.id.maxInfoTextView);
        max.setText(Integer.toString(forecast.getMax()));
        TextView humidity = (TextView)view.findViewById(R.id.humidityInfoTextView);
        humidity.setText(Integer.toString(forecast.getHumidity()));
        TextView speed = (TextView)view.findViewById(R.id.speedInfoTextView);
        speed.setText(Integer.toString(forecast.getSpeed()));
        TextView pressure = (TextView)view.findViewById(R.id.pressureInfoTextView);
        pressure.setText(Integer.toString(forecast.getPressure()));

        return view;
    }
}
