package com.xarrper.olga.myweather;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Http {

// TODO: исправить на константы!
//    private static final String ENDPOINT = "http://api.openweathermap.org/data/2.5/forecast/daily";
//
//    private static final String KEY_G = "g";
//    private static final String KEY_UNITS = "units";
//    private static final String KEY_APPID = "APPID";
//    private static final String KEY_CNT = "cnt";
//
//    private static final String VALUE_G = "Astrakhan";
//    private static final String VALUE_UNITS = "metric";
//    private static final String VALUE_APPID = "2d98d7f1d9012a6b3f2a1262ddc464d0";
//    private static final String VALUE_CNT = "7";
//
//    private String fetchItems() {
//        return Uri.parse(ENDPOINT).buildUpon()
//                .appendQueryParameter(KEY_G, VALUE_G)
//                .appendQueryParameter(KEY_UNITS, VALUE_UNITS)
//                .appendQueryParameter(KEY_APPID, VALUE_APPID)
//                .appendQueryParameter(KEY_CNT, VALUE_CNT)
//                .build().toString();
//    }

    private static final String TAG = "Http";

    private String fetchItems(String param) {
        String str = Uri.parse("http://api.openweathermap.org/data/2.5/forecast/daily").buildUpon()
                .appendQueryParameter("q", param)
                .appendQueryParameter("units", "metric")
                .appendQueryParameter("APPID", "2d98d7f1d9012a6b3f2a1262ddc464d0")
                .appendQueryParameter("cnt", "7")
                .build().toString();
        Log.d(TAG, "fetchItems : "  + str);
        return Uri.parse("http://api.openweathermap.org/data/2.5/forecast/daily").buildUpon()
            .appendQueryParameter("q", param)
            .appendQueryParameter("units", "metric")
            .appendQueryParameter("APPID", "2d98d7f1d9012a6b3f2a1262ddc464d0")
            .appendQueryParameter("cnt", "7")
            .build().toString();
    }

    byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            Log.d(TAG, "getUrlBytes: " + new String(out.toByteArray()));
            return out.toByteArray();
        } catch (Exception e) {
            Log.d(TAG, "getUrlBytes: ошибка" + e);
            return null;
        }
        finally {
            Log.d(TAG, "getUrlBytes: finally");
            connection.disconnect();
        }
    }

    public String getUrl(String param) throws IOException {
        return new String(getUrlBytes(fetchItems(param)));
    }
}
