package com.xarrper.olga.myweather;

import android.app.Activity;
import android.app.AlertDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;

//TODO: SharedPreferences - отдельный класс.
public class ChoiceCityDialogFragment extends DialogFragment {

    public static final String EXTRA_CITY =
            "com.xarrper.olga.myweather.city";
    private SharedPreferences prefs;
    private String city;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        prefs = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

        View v = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_choice_city, null);

        final EditText cityTextView = (EditText)v.findViewById(R.id.city_edit_text);
        cityTextView.setText(prefs.getString("city", "Astrakhan"));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_choice_city)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        city = cityTextView.getText().toString();
                        prefs.edit().putString("city", city).commit();
                        sendResult(Activity.RESULT_OK);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sendResult(Activity.RESULT_CANCELED);
                    }
                })
                .setView(v);
        return builder.create();
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null)
            return;
        Intent i = new Intent();
        i.putExtra(EXTRA_CITY, city);
        getTargetFragment()
                .onActivityResult(getTargetRequestCode(), resultCode, i);
    }

}
